import axios from "axios";

const KEY = "AIzaSyBUrAZ3FNr8ZMvLb0CgYGXEAnnC6gzD8-U";

export default axios.create({
  baseURL: "https://www.googleapis.com/youtube/v3",
  params: {
    part: "snippet",
    maxResults: 5,
    key: KEY
  }
});
