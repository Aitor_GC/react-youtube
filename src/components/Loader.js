import React from "react";

const Loader = props => {
  return (
    <div class="loader ui segment">
      <div class="ui active inverted dimmer">
        <div class="ui text loader">{props.text}</div>
      </div>
      <p />
    </div>
  );
};

export default Loader;
