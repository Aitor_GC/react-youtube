import React from "react";
import SearchBar from "./SearchBar";
import VideoList from "./VideoList";
import VideoDetail from "./VideoDetail";
import youtube from "../apis/youtube";

class App extends React.Component {
  state = {
    videos: [],
    selectedVideo: null
  };

  componentDidMount() {
    this.onSearchSubmit("top views");
  }

  onSearchSubmit = async term => {
    const response = await youtube.get("/search", {
      params: {
        q: term
      }
    });

    this.setState({ videos: response.data.items, selectedVideo: null });
  };

  onVideoSelect = video => {
    this.setState({ selectedVideo: video });
  };

  renderContent() {
    if (!this.state.selectedVideo) {
      return (
        <div className="ui row">
          <div className="sixteen wide column">
            <VideoList
              onVideoSelect={this.onVideoSelect}
              videos={this.state.videos}
            />
          </div>
        </div>
      );
    }
    return (
      <div className="ui row">
        <div className="eleven wide column">
          <VideoDetail video={this.state.selectedVideo} />
        </div>
        <div className="five wide column">
          <VideoList
            onVideoSelect={this.onVideoSelect}
            videos={this.state.videos}
          />
        </div>
      </div>
    );
  }

  render() {
    return (
      <div className="ui container" style={{ marginTop: "10px" }}>
        <SearchBar
          title="Recreación de un sitio web basado en componentes"
          placeholder="Buscar"
          onSubmit={this.onSearchSubmit}
        />
        <div className="ui grid">{this.renderContent()}</div>
      </div>
    );
  }
}

export default App;
